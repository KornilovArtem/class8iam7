﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class Wwisebreathing : MonoBehaviour
{
    vThirdPersonController tpController;
    public AK.Wwise.Event breathEvent;
    
    // Start is called before the first frame update
    void Start()
    {
        tpController = GetComponent<vThirdPersonController>();
        breathEvent.Post(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(tpController.currentStamina);
        AkSoundEngine.SetRTPCValue("Stamina", tpController.currentStamina, gameObject);
    }
}
