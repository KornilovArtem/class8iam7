﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class CommonWwiseFootsteps : MonoBehaviour
{
    public GameObject LeftFoot;
    public GameObject RightFoot;
    public AK.Wwise.Event FootstepsEvent;
    public LayerMask LayerMask;

    vThirdPersonInput tpInput;
    vThirdPersonController tpController;
            
    // Start is called before the first frame update
    void Start()
    {
        tpInput = GetComponent<vThirdPersonInput>();
        tpController = GetComponent<vThirdPersonController>();

    }


    void footstep_walking(string arg)
    {
        if(tpInput.cc.inputMagnitude > 0.1)
        {
            if (arg == "left")
            {
                Playfootstep(LeftFoot);
            }
            else if (arg == "right")
            {
                Playfootstep(RightFoot);
            }
        }
            
    }
    void Playfootstep(GameObject footObject) // функция проверки поверхности для  нужного геймобъекта
    {
        if (Physics.Raycast(footObject.transform.position, Vector3.down, out RaycastHit hit, 0.3f, LayerMask)) // запускаем рейкаст из объекта нужной ноги вниз
        {
            AkSoundEngine.SetSwitch("SWITCH_surface", hit.collider.tag, footObject);  // выставляем свитч нужной свитч-группы в положение такое же как тэг поверхности,  на которую наступила нога, применяем свитч для нужной ноги

            if (tpController.isSprinting)
            {
                AkSoundEngine.SetSwitch("SWITCH_Locomotion", "Running", footObject);
            }
            else AkSoundEngine.SetSwitch("SWITCH_Locomotion", "Walking", footObject);
            
            FootstepsEvent.Post(footObject); // запускаем ивент для из нужной ноги
            
           
        }
    }
}
