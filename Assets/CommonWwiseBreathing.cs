﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class CommonWwiseBreathing : MonoBehaviour
{
    public AK.Wwise.Event breathingEvent;
    public AK.Wwise.Event RestorationEvent;
    vThirdPersonController tpController;
    vThirdPersonInput tpInput;
    //public bool isStaminaUp = true;
    public bool isStaminaRestoring = false;
    
    // Start is called before the first frame update
    void Start()
    {
        tpInput = GetComponent<vThirdPersonInput>();
        tpController = GetComponent<vThirdPersonController>();
        breathingEvent.Post(gameObject);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Debug.Log(tpController.currentStamina);
        LocomotionCheck();
        StaminaCheck();
        AkSoundEngine.SetRTPCValue("RTPC_ext_stamina", tpController.currentStamina, gameObject);
    }
    void StaminaCheck()
    {
        //if (tpController.currentStamina <= 0.1 & isStaminaUp)
        //if (tpController.currentStamina <= 0.1)
        //{
        //    isStaminaRestoring = true;
        //    //isStaminaUp = false;
        //    RestorationEvent.Post(gameObject);
        //}
        //if (tpController.currentStamina >= 200 & !isStaminaUp)
        if (tpController.currentStamina >= 200 & isStaminaRestoring) // Проверяем на то что наша стамина заполнилась и что он а до этого восстанавливалась
        {
            isStaminaRestoring = false; //  состояние меняется на то что стамина больше не восстанавливается
            //isStaminaUp = true;
            breathingEvent.Post(gameObject); // запускает ивент обычного дыхания
        }




    }
    void LocomotionCheck() // проверка  в каком состоянии сейчас персонаж
    {
        if (tpInput.cc.inputMagnitude < 0.1) 
        {
            AkSoundEngine.SetSwitch("SWITCH_Locomotion", "Idle", gameObject);
        }
        else if (tpInput.cc.inputMagnitude >= 0.1)
        {
            if (tpController.isSprinting)
            {
                if (isStaminaRestoring) // Проверяем состояние восстановления стамины
                {
                    breathingEvent.Post(gameObject); // запускаем ивент  обычного дыхания при беге
                    isStaminaRestoring = false; // Состояние меняет на то, что стамина больше не восстанавливает, если вс случае до этого она восстанавливалась
                }
                AkSoundEngine.SetSwitch("SWITCH_Locomotion", "Running", gameObject); // передается  свитч  локомоушн  бега.
            }
            else
            {
                if (tpController.currentStamina < 200 & !isStaminaRestoring) 
                {
                    RestorationEvent.Post(gameObject);
                    isStaminaRestoring = true;
                }
                AkSoundEngine.SetSwitch("SWITCH_Locomotion", "Walking", gameObject);
                
            }
        }

        

    }
}
