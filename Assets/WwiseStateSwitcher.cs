﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WwiseStateSwitcher : MonoBehaviour
{
    public bool trigger;
    private bool state = true;
    public Collider player;

    private void OnTriggerEnter(Collider other)
    {
        if (other == player)
        {
            trigger = !trigger;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (trigger && !state)
        {
            AkSoundEngine.SetState("STATE_room_01", "Inside");
            state = true;
        }
        else if (!trigger && state)

        {
            AkSoundEngine.SetState("STATE_room_01", "Outside");
            state = false;
        }
    }
}
